SELECT t.train_id, sd.city, TO_CHAR(t.departure_time, ‘DD/MM/YYYY HH24:MI’), sa.city, TO_CHAR(t.arrival_time, ‘DD/MM/YYYY HH24:MI’)
FROM t_train t
JOIN t_station sd
ON sd.station_id = t.departure_station_id
JOIN t_station sa
ON sa.station_id = t.arrival_station_id
ORDER BY t.train_id;