SELECT *
FROM(SELECT DISTINCT (t.train_id) "train-ID"  ,
 sta.city||'-'||stb.city "train-name", COUNT(ticket_id) "nbr-tickets"
FROM t_train t
	JOIN t_station sta
	ON t.departure_station_id = sta.station_id
	JOIN t_station stb
	ON t.arrival_station_id = stb.station_id
	JOIN t_wagon_train twt
	ON t.train_id=twt.train_id
	JOIN t_ticket ti
	ON ti.wag_tr_id = twt.wag_tr_id
GROUP BY t.train_id, sta.city, stb.city
ORDER BY COUNT(ticket_id) DESC)
WHERE ROWNUM <=5; 