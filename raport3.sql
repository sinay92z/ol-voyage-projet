SELECT*
FROM(SELECT r.reservation_id
"N° RESERVATION", r.creation_date
"CREATION", e.last_name ||''|| e.first_name
"EMPLOYE", c.last_name ||''|| c.first_name
"CLIENT"
FROM t_reservation r
JOIN t_employee e
ON r.reservation_id = e.employee_id
JOIN t_customer c 
ON r.buyer_id = c.customer_id
GROUP BY r.reservation_id, r.creation_date, e.last_name, e.first_name, c.last_name, c.first_name
ORDER BY (r.creation_date))
WHERE ROWNUM <2;