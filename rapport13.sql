TO_CHAR(t.departure_time, ‘DD/MM/YYYY HH24:MI’)||' '|| sa.station_id ||' '||
TO_CHAR(t.arrival_time, ‘DD/MM/YYYY HH24:MI’) as "Train"
FROM t_train t
	JOIN t_station sd 
	ON sd.station_id = t.departure_station_id
	JOIN t_station sa
	ON sa.station_id = t.arrival_station_id
	JOIN t_wagon_train twt
	ON twt.train_id=t.train_id
	JOIN t_wagon tw 
	ON tw.wagon_id = twt.wagon_id
	JOIN t_ticket tt 
	ON  tt.wag_tr_id = twt.wag_tr_id
	JOIN t_reservation tr
	ON tr.reservation_id=tt.reservation_id
	JOIN t_customer tc
	ON tc.customer_id = tr.buyer_id
WHERE tw.class_type = 1
	AND departure_time BETWEEN '20/10/2020' AND '26/10/2020'
	AND birth_date BETWEEN'20/10/95' AND  SYSDATE  
ORDER BY tr.creation_date;