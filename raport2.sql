SELECT DISTINCT c.last_name, c.first_name
FROM t_customer c
JOIN t_reservation r
ON r.buyer_id = c.customer_id
JOIN t_ticket ti
ON ti.reservation_id = r.reservation_id
WHERE r.buyer_id <> ti.customer_id
ORDER BY c.last_name, c.first_name;