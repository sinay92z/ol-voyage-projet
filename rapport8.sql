SELECT c.last_name, c.first_name, c.address
FROM t_customer c
WHERE c.pass_id IS NULL
ORDER BY c.last_name, c.first_name;