SELECT COUNT(c.customer_id)
FROM t_customer c
JOIN t_pass p
ON p.pass_id = c.pass_id
JOIN t_reservation r
ON r.buyer_id = c.customer_id
JOIN t_ticket ti
ON ti.reservation_id = r.reservation_id
JOIN t_wagon_train wag_tr
ON wag_tr.wag_tr_id = ti.wag_tr_id
JOIN t_train t
ON t.train_id = wag_tr.wag_tr_id
WHERE p.pass_name = ‘Senior’ AND buy_method IS NOT NULL
AND TO_CHAR(t.departure_time, ‘MM/YYYY’) = '10/2020';