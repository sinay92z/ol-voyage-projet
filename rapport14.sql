SELECT t.train_id, tsd.city|| '-'|| tsa.city "TRAIN-NAME",
sum(tw.nb_seat)-COUNT(tt.ticket_id) "places libres"
	FROM t_train t
	JOIN t_station tsd 
	ON t.departure_station_id= tsd.station_id
	JOIN t_station tsa
	ON t.arrival_station_id=tsa.station_id
	JOIN t_wagon_train twt
	ON t.train_id=twt.train_id
	JOIN t_wagon tw 
	ON twt.wagon_id= tw.wagon_id
	JOIN t_ticket tt
	ON twt.wag_tr_id=tt.wag_tr_id
WHERE twt.train_id in (select t.train_id from t_train)
AND t.distance > 300
AND t.departure_time LIKE '22/10/20'
GROUP BY t.train_id, tsd.city, tsa.city
ORDER BY t.train_id;